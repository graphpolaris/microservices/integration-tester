# Integration tester




## Function
This service uses rabbitMQ to test functionality of individual services. 
When a config file is submitted to datastrophe.science.uu.nl/integration-test it sends the messages defined under producer to the specified routing key. It then listens for the response on the specified consumer and checks if it matches the defined response.
The integration tester expects json in escaped string format, but checks the content of these strings on meaning, not strict string comparison (like older versions)
Tests can be automated following the instructions below.


## How to setup the environment
Use the deployment in the config files repo, or directly from the dev server in the kubernetes folder.

## How to setup tests

Make a build stage that pushes the image to something like your-service-name-staging:latest.
The word staging isn’t mandatory, but it does make for clearer function.

Then make a folder called integration in the root of your project in place in there:

-a pod.yml file, containing the info you would normally put in a deployment. Make sure the your-service-name-staging:latest is used as image instead

-a config.json file containing the integration tests. The format is pretty self explanatory and examples can be read from the other services. The basic concept is you make a producer (which sends messages to your service as specified) and a consumer (which receives messages from your consumer, and expects them as specified)
This specifications for sending and receiving are in json, but with the quotes escaped with a \ character.
Json is parsed on the tester side, so different orders of entries in the json shouldn’t matter, as long as it’s meaning stays identical.

make an integration stage that copies over the pod.yml and config.json over to the server. It’s best to just copy paste this stage from something like query-service and edit as needed.


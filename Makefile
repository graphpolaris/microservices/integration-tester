.PHONY: all dep build test lint

lint: dep ## Lint the files
	@golint -set_exit_status ./...

test: ## Run unittests
	@go test -cover -coverprofile=coverage.txt -covermode count ./...

race: dep ## Run data race detector
	@go test -race -short ./...

dep: ## Get the dependencies
	@go get -v -d ./...
	@go get -u golang.org/x/lint/golint
	@go get -u github.com/boumenot/gocover-cobertura

coverage: dep
	@go test -v -coverpkg=./... -coverprofile=cover.out ./...
	@go tool cover -func cover.out | grep total
	@go tool cover -html=cover.out -o cover.html

windows:
	$(eval export GOOS := windows)
	@go build -o main main.go

macos:
	$(eval export GOOS := darwin)
	@go build -o main main.go

#	CGO_ENABLED=0 go build -o main main.go
linux: # Build for linux
	$(eval export GOOS := linux)
	go build -o main main.go
run:
	./builds/main

docker:
	make linux
	@docker build -t datastropheregistry.azurecr.io/integration-tester:latest .
	@docker push datastropheregistry.azurecr.io/integration-tester:latest

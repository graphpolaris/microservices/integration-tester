module rabbitmq-integration-tester

go 1.15

require (
	github.com/go-redis/redis/v8 v8.8.0
	github.com/onsi/gomega v1.11.0 // indirect
	github.com/streadway/amqp v1.0.0
	github.com/thijsheijden/alice v0.1.15
	github.com/wI2L/jsondiff v0.1.0
)

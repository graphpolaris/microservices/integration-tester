/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package main

import (
	"log"
	"rabbitmq-integration-tester/internal/testhandler"
	"rabbitmq-integration-tester/internal/web"
)

/*
This is the main method, it executes the code for this service
*/
func main() {
	log.Println("Hey docker!")
	testhandler.CreateBroker()
	web.StartListener()
}

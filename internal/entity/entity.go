/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

import "sync"

/*
A Config describes the config of the tester
*/
type Config struct {
	Timeout    int               `json:"timeout"`
	Producers  []Producer        `json:"producers"`
	Consumers  []Consumer        `json:"consumers"`
	Assertions []*Assertion      `json:"assertions"`
	RedisSet   map[string]string `json:"redisSet"`
}

/*
A struct that describes a consumer
*/
type Consumer struct {
	ID           string `json:"id"`
	Exchange     string `json:"exchange"`
	Exchangetype string `json:"exchangeType"`
	Queue        string `json:"queue"`
	Routingkey   string `json:"routingKey"`
}

/*
A struct that describes a produces
*/
type Producer struct {
	ID           string    `json:"id"`
	Exchange     string    `json:"exchange"`
	Exchangetype string    `json:"exchangeType"`
	Messages     []Message `json:"messages"`
}

/*
A struct that describes a message
*/
type Message struct {
	RoutingKey string            `json:"routingKey"`
	Headers    map[string]string `json:"headers"`
	Data       string            `json:"data"`
}

/*
A struct that describes an assertion
*/
type Assertion struct {
	Type       string          `json:"type"`
	ConsumerID string          `json:"consumerid"`
	ID         string          `json:"id"`
	Data       string          `json:"data"`
	Asserted   bool            `json:"-"`
	WG         *sync.WaitGroup `json:"-"`
	Object     interface{}     `json:"-"`
}

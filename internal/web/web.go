/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package web

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"rabbitmq-integration-tester/internal/entity"
	"rabbitmq-integration-tester/internal/testhandler"
)

/*
StartListener starts listening
*/
func StartListener() {
	http.HandleFunc("/start/", handleUpload)
	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/", fs)
	http.ListenAndServe(":6969", nil)
}

/*
handleUpload handles the upload
	w: http.ResponseWriter, writes the responses for http
	r: *http.request, sends the requests for http
*/
func handleUpload(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	r.ParseMultipartForm(32 << 20)
	formFile, _, err := r.FormFile("configFile")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer formFile.Close()
	defer os.Remove("logfile")

	var config entity.Config
	err = json.NewDecoder(formFile).Decode(&config)
	if err != nil {
		log.Println(err)
		http.Error(w, "invalid request json", http.StatusBadRequest)
		return
	}

	log.Println("Going into integration test")
	err = testhandler.HandleTest(config)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.ServeFile(w, r, "logfile")
}

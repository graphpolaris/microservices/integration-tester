/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package testhandler

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"rabbitmq-integration-tester/internal/entity"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/streadway/amqp"
	"github.com/thijsheijden/alice"
)

var broker *alice.RabbitBroker
var assertions map[string]*entity.Assertion
var producers map[string]*alice.Producer
var consumers map[string]*alice.Consumer
var brokerAvailable bool = false

/*
Creates a broker
*/
func CreateBroker() {
	var err error
	// Open broker connection
	config := alice.DefaultConfig
	config.SetHost("rabbitmq")
	broker, err = alice.CreateBroker(config)
	if err != nil {
		log.Println(err)

		ticker := time.NewTicker(time.Second * 5)
		done := make(chan bool, 1)
		for {
			select {
			case <-ticker.C:
				log.Println("Retrying broker connection")
				broker, err = alice.CreateBroker(config)
				if err == nil {
					done <- true
					ticker.Stop()
				}
			case <-done:
				log.Println("Succesfully connected to broker")
				return
			}
		}
	}
	brokerAvailable = true
}

/*
Handles the tests
	Return: error, a potential error
*/
func HandleTest(config entity.Config) error {

	if !brokerAvailable {
		log.Println("Broker not yet available")
		return errors.New("Broker not yet available")
	}

	log.Print("Starting integration test")

	var wg sync.WaitGroup
	c := make(chan struct{})

	// Turn on logging in Alice
	alice.SetLogging()

	// Create new log file
	file, err := os.OpenFile("logfile", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0664)
	if err != nil {
		log.Println(err)
		panic(err)
	}
	defer file.Close()

	log.SetOutput(file)

	// Convert assertion data to byte format
	for _, a := range config.Assertions {
		a.Asserted = false
		jsonString := strings.Replace(a.Data, "'", "\"", -1)
		a.Data = jsonString

		var object interface{}
		err = json.Unmarshal([]byte(jsonString), &object)

		if err != nil {
			log.Println(err)
		}

		// Setup waitgroup
		wg.Add(1)
		a.WG = &wg
	}

	// Open Redis connection and set all key value pairs
	setupRedisValues(config.RedisSet)

	// Initialize maps
	assertions = make(map[string]*entity.Assertion)
	producers = make(map[string]*alice.Producer)
	consumers = make(map[string]*alice.Consumer)

	// Add all assertions to the assertions map
	for _, a := range config.Assertions {
		assertions[a.ID] = a
	}

	// Start creating the requested consumers
	for _, c := range config.Consumers {
		createConsumer(c)
	}

	// Start creating the requested producers
	for _, p := range config.Producers {
		createProducer(p)
	}

	// Produce the messages
	for _, p := range config.Producers {
		for _, m := range p.Messages {
			// Replace all single quotes with double quotes
			message := strings.Replace(m.Data, "'", "\"", -1)
			// Load in message
			bMessage := []byte(message)

			// Create header table
			headers := amqp.Table{}
			for k, v := range m.Headers {
				headers[k] = v
			}

			pro := *producers[p.ID]

			// Publish the message
			pro.PublishMessage(bMessage, &m.RoutingKey, &headers)
		}
	}

	// Create timeout ticker
	ticker := time.NewTicker(time.Second * time.Duration(config.Timeout))

	go func() {
		defer close(c)
		wg.Wait()
	}()

	// Wait until we reach the timeout or all assertions are completed
	select {
	case <-ticker.C:
		break
	case <-c: // Waitgroup is done
		break
	}

	file.WriteString("\n")
	numAsserted := 0
	for assertionID, a := range assertions {
		if a.Asserted {
			numAsserted++
		}
		file.WriteString(assertionID + " -> " + strconv.FormatBool(a.Asserted))
		if !a.Asserted {
			file.WriteString("\n" + a.Data + "\n")
		}
		file.WriteString("\n")
	}
	file.WriteString("\nTotal: " + fmt.Sprint(numAsserted) + "/" + fmt.Sprint(len(assertions)) + " asserted")

	if numAsserted < len(assertions) {
		file.WriteString("\nFAIL")
	} else {
		file.WriteString("\nSUCCESS")
	}

	log.SetOutput(os.Stdout)

	// Shutdown producers and consumers
	for _, producer := range producers {
		p := *producer
		p.Shutdown()
	}

	for _, consumer := range consumers {
		c := *consumer
		c.Shutdown()
	}

	return nil
}

/*
setupRedisValues sets up the values for redis
	kvp: map[string]string, the key value pairs for the setup
*/
func setupRedisValues(kvp map[string]string) {
	redisClient := redis.NewClient(&redis.Options{
		Addr: "redis:6379",
	})

	// Set all kvp for routing
	for k, v := range kvp {
		status := redisClient.Set(context.Background(), k, v, 0)
		if status.Err() != nil {
			log.Println(status.Err().Error())
		}
	}
}

/*
createConsumer creates a consumer
	c: entity.Consumer, the consumer to be created
*/
func createConsumer(c entity.Consumer) {
	exchange, err := alice.CreateExchange(c.Exchange, alice.ExchangeType(c.Exchangetype), true, false, false, false, nil)
	if err != nil {
		log.Println(err)
		panic(err)
	}

	queue := alice.CreateQueue(exchange, c.Queue, false, false, true, false, nil)

	ac, err := broker.CreateConsumer(queue, c.Routingkey, alice.DefaultConsumerErrorHandler)
	if err != nil {
		log.Println(err)
	}

	go ac.ConsumeMessages(nil, c.ID, true, handleMessage)

	consumers[c.ID] = &ac
}

/*
createProducer creates a producer
	p: entity.producer, the producer to be created
*/
func createProducer(p entity.Producer) {
	exchange, err := alice.CreateExchange(p.Exchange, alice.ExchangeType(p.Exchangetype), true, false, false, false, nil)
	if err != nil {
		log.Println(err)
		panic(err)
	}

	ap, err := broker.CreateProducer(exchange, alice.DefaultProducerErrorHandler)
	if err != nil {
		log.Println(err)
	}

	producers[p.ID] = &ap
}

/*
handleMessage handles the message
	msg: amqp.Delivery, the message to be handled
*/
func handleMessage(msg amqp.Delivery) {
	// Fix up msg body
	msgBody := string(msg.Body)

	//debug flags for forcing check
	text := false
	json := true

	for _, a := range assertions {
		// If this assertion is not asserted yet and this message was meant for this consumer
		if !a.Asserted && msg.ConsumerTag == a.ConsumerID {
			if text { //if string assertion
				if handleStringAssertion(msgBody, a) {
					log.Println("Assertion '" + a.ID + "' asserted")
					a.Asserted = true
					a.WG.Done()
					return
				}
			} else if json { //if json assertion / uses sData format
				if handleComplexAssertion(msg, msgBody, a) {
					log.Println("Assertion '" + a.ID + "' asserted recursively")
					println("Assertion '" + a.ID + "' asserted recursively")
					a.Asserted = true
					a.WG.Done()
					return
				}
			}

		}
	}

	// Message did not fit any assertion
	log.Println("Message: \n\n" + msgBody + "\n\n did not match any assertion.")
}

func handleStringAssertion(msgBody string, a *entity.Assertion) bool {
	return strings.TrimSpace(a.Data) == msgBody
}
func handleComplexAssertion(msg amqp.Delivery, msgBody string, a *entity.Assertion) bool {
	return handleComplexAssertionHelper(specialUnmarshall([]byte(msgBody)), specialUnmarshall([]byte(a.Data)))
}

func handleComplexAssertionHelper(msgBody map[string]interface{}, a map[string]interface{}) bool {
	assertionData := a      //specialUnmarshal(a) //a.SData
	receivedData := msgBody //specialUnmarshal(msgBody)

	for key, value := range assertionData {
		// fmt.Println("key:", key, "value:", value)

		receivedValue := receivedData[key]
		switch receivedValue.(type) {
		default:
			if bytes.Equal(specialMarshall(receivedData[key]), specialMarshall(value)) {
				print("key: " + key + " succeeded with received value: ")
				println(receivedData[key])
			} else {
				// println("assertion key: " + key + " expected value: " + value + " but got: ")
				println("assertion key: " + key + " failed!")
				print("expected: ")
				println(value)
				print("got: ")
				println(receivedData[key])
				return false
			}

			// fmt.Printf("unexpected type %T", v)
		case map[string]interface{}:
			if handleComplexAssertionHelper((receivedValue.(map[string]interface{})), (value.(map[string]interface{}))) {
			} else {
				return false
			}
		}
	}
	return true

}
func specialMarshall(data interface{}) []byte {
	str, err := json.Marshal(data)
	if err != nil {
		println("specialMarshall failed")
		println(err)
		return nil
	}
	return str
}

func specialUnmarshall(msg []byte) map[string]interface{} {
	//source: https://socketloop.com/tutorials/golang-decode-unmarshal-unknown-json-data-type-with-map-string-interface
	// msg := "{\"assets\" : {\"old\" : 123}}"

	// We only know our top-level keys are strings
	mp := make(map[string]interface{})

	// Decode JSON into our map
	err := json.Unmarshal([]byte(msg), &mp)
	if err != nil {
		println("specialUnmarshall failed")

		println(err)
		return nil
	}

	// Iterate the map and print out the elements one by one
	// Note: that mp has to be deferenced here or range will fail
	// for key, value := range mp {
	// fmt.Println("key:", key, "value:", value)
	// }
	return mp
}
